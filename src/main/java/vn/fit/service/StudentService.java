package vn.fit.service;

import java.util.List;

import vn.fit.entity.Student;

public interface StudentService {
	List<Student> getList();

	Student getById(String id);

	Student create(Student student);
	
	Student update(String id, Student student);

	void deleteById(String id);
	
	void deleteSelectedById(String[] ids);
}
