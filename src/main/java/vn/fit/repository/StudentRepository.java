package vn.fit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import vn.fit.entity.Student;

public interface StudentRepository extends JpaRepository<Student, String> {

	@Modifying
	@Query(value = "delete from Student s where s.studentid in ?1")
	void deleteStudentsWithIds(List<String> ids);
}
